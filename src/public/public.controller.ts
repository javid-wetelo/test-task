import { Controller, Get } from '@nestjs/common';
import { ApiOperation } from '@nestjs/swagger';
import { Publicv2Service } from './public.service';

@Controller()
export class Publicv2Controller {
  constructor(private readonly publicService: Publicv2Service) {}
  @ApiOperation({ summary: 'Empty operation for now' })
  @Get()
  async getInfo() {
    return 1;
  }
}
