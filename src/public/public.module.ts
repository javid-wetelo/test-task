import { Module } from '@nestjs/common';
import { Publicv2Controller } from './public.controller';
import { Publicv2Service } from './public.service';

@Module({
  controllers: [Publicv2Controller],
  providers: [Publicv2Service],
})
export class Publicv2Module {}
