import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { TaskStatus } from '../enums/taskStatus.enum';

export class TaskDto {
  @ApiProperty({
    example: 'I did not figure out what kind should this ID be',
  })
  @IsString()
  ID: string;

  @ApiProperty({
    example: 'task1',
  })
  @IsString()
  name: string;

  @ApiProperty({
    enum: TaskStatus,
    example: TaskStatus.created,
  })
  @IsEnum(TaskStatus)
  status: TaskStatus;
}
