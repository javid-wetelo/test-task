import { Controller } from '@nestjs/common';
import { TaskService } from './task.service';
import {
  Ctx,
  MessagePattern,
  Payload,
  RmqContext,
} from '@nestjs/microservices';
import { TaskEvent } from './enums/taskEvent.enum';
import { TaskDto } from './dto/task.dto';

@Controller()
export class TaskController {
  constructor(private readonly taskService: TaskService) {}
  @MessagePattern({ cmd: TaskEvent.TASK_CREATED })
  async handleTaskCreationEvent(
    @Ctx() context: RmqContext,
    @Payload() payload: TaskDto,
  ) {
    const channel = context.getChannelRef();
    const originalMsg = context.getMessage();

    const task = await this.taskService.createTask(payload);
    channel.ack(originalMsg);
    return task.id;
  }
}
