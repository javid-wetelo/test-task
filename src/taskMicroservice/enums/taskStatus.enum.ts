export enum TaskStatus {
  created = '0',
  updated = '1',
  deleted = '2',
}
