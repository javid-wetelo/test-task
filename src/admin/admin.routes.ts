import { TaskProducerModule } from './task/task.module';

export const adminRoutes = [
  {
    path: 'task',
    module: TaskProducerModule,
  },
];
