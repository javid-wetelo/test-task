import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';
import { TaskProducerModule } from './task/task.module';

@Module({
  imports: [TaskProducerModule],
  controllers: [AdminController],
  providers: [],
})
export class AdminModule {}
