import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';

@Injectable()
export class TaskEmitterService {
  constructor(@Inject('TASK_SERVICE') private taskqueueClient: ClientProxy) {}

  async sendTaskToQueue({ data, cmd }) {
    return this.taskqueueClient.send({ cmd }, data);
  }
}
