import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { TaskController } from './task.controller';
import { TaskEmitterService } from './taskEmitter.service';

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: 'TASK_SERVICE',
        imports: [ConfigModule],
        useFactory: async (configService: ConfigService) => {
          const user = configService.get('RABBITMQ_USER');
          const password = configService.get('RABBITMQ_PASSWORD');
          const host = configService.get('RABBITMQ_HOST');

          const rmqQueue = configService.get('RABBITMQ_QUEUE_NAME');
 
          const rmqUrl = `amqp://${host}`;

          return {
            transport: Transport.RMQ,
            urls: [rmqUrl],
            queue: rmqQueue,
            noAck: false,
            queueOptions: {
              durable: true,
            },
          };
        },
        inject: [ConfigService],
      },
    ]),
  ],
  controllers: [TaskController],
  providers: [TaskEmitterService],
})
export class TaskProducerModule {}
