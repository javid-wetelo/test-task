import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsString } from 'class-validator';
import { TaskStatus } from '../enum/taskStatus.enum';

export class TaskDto {
  @ApiProperty({
    type: String,
    description: 'requeired ID',
  })
  @IsString()
  ID: string;

  @ApiProperty()
  @IsString()
  name: string;

  @ApiProperty({
    enum: TaskStatus,
    isArray: true,
    example: [TaskStatus.created, TaskStatus.updated],
  })
  @IsEnum(TaskStatus)
  status: TaskStatus;
}
