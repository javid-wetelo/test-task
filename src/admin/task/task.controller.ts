import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {
  ApiBearerAuth,
  ApiCreatedResponse,
  ApiForbiddenResponse,
  ApiTags,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { TaskDto } from './dto/task.dto';
import { TaskEvent } from './enum/taskEvent.enum';
import { TaskEmitterService } from './taskEmitter.service';

@ApiBearerAuth('authorization')
@ApiTags('task')
@Controller()
export class TaskController {
  constructor(private readonly taskEmitterService: TaskEmitterService) {}

  @ApiCreatedResponse({ description: 'Created Succesfully' })
  @ApiUnprocessableEntityResponse({ description: 'Bad Request' })
  @ApiForbiddenResponse({ description: 'Unauthorized Request' })
  @UseGuards(AuthGuard('jwt'))
  @Post()
  async createTask(@Body() task: TaskDto) {
  
    const response = await this.taskEmitterService.sendTaskToQueue({
      data: task,
      cmd: TaskEvent.TASK_CREATED,
    });

    return response;
  }
}
