import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiOperation, ApiTags } from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('admin')
@Controller()
export class AdminController {
  @ApiOperation({ summary: 'Empty operation for now' })
  @UseGuards(AuthGuard('jwt'))
  @Get()
  public async checkController() {
    return 0;
  }
}
