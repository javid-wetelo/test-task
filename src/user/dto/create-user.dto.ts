import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty, IsString, Length } from 'class-validator';
import { Match } from '../decorator/matchPassword.decorator';

export class CreateUserDto {
  @ApiProperty()
  @IsString()
  @IsEmail()
  email: string;

  @ApiProperty()
  @Length(8, 16)
  @IsNotEmpty()
  password: string;

  @ApiProperty()
  @Length(8, 16)
  @IsNotEmpty()
  @Match('password')
  confirmPassword: string;
}
