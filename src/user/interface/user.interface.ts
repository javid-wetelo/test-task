export interface IUser {
  id: number;
  email: string;
  password: string;
  refreshToken: string;
}

export type IUserPayload = Omit<IUser, 'password' | 'refreshToken'>;
