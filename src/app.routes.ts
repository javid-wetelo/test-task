import { Routes } from '@nestjs/core';
import { AdminModule } from './admin/admin.module';
import { adminRoutes } from './admin/admin.routes';
import { Publicv2Module } from './public/public.module';

export const appRoutes: Routes = [
  {
    path: 'admin',
    module: AdminModule,
    children: adminRoutes,
  },
  {
    path: 'v2',
    module: Publicv2Module,
    children: [] /** Here will be public routes */,
  },
];
