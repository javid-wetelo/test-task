import {
  Body,
  Controller,
  HttpCode,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { RequestWithUser } from 'src/interfaces/request-with-user.interface';
import { CreateUserDto } from 'src/user/dto/create-user.dto';
import { AuthenticationService } from './authentication.service';

@ApiTags('auth')
@Controller('auth')
export class AuthenticationController {
  constructor(private readonly authService: AuthenticationService) {}

  @UseGuards(AuthGuard('local'))
  @HttpCode(200)
  @Post('signin')
  async login(@Req() req: RequestWithUser) {
    return await this.authService.login(req.user);
  }

  @Post('signup')
  async signUp(@Body() user: CreateUserDto) {
    return await this.authService.create(user);
  }

  @ApiBearerAuth('authorization')
  @UseGuards(AuthGuard('jwt-refresh'))
  @HttpCode(200)
  @Post('refresh')
  async refresh(@Req() req: RequestWithUser) {
    return await this.authService.login(req.user);
  }

  @ApiBearerAuth('authorization')
  @UseGuards(AuthGuard('jwt'))
  @Post('logout')
  @HttpCode(200)
  async logout(@Req() req: RequestWithUser) {
    return await this.authService.logout(req.user.id);
  }
}
