import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { Request } from 'express';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { UserService } from 'src/user/user.service';
import { IUserPayload } from 'src/user/interface/user.interface';
import { jwtEnv } from '../../config/index.config';

@Injectable()
export class RefreshTokenStrategy extends PassportStrategy(
  Strategy,
  'jwt-refresh',
) {
  constructor(
    private readonly configService: ConfigService,
    private readonly userService: UserService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: jwtEnv.JWT_REFRESH_SECRET,
      passReqToCallback: true,
    });
  }

  async validate(req: Request, payload: IUserPayload) {
    const refreshToken = req.get('Authorization').replace('Bearer', '').trim();
    if (!refreshToken) throw new UnauthorizedException('Missing refresh token!');
    const user = await this.userService.getUserIfRefreshTokenMatches(
      refreshToken,
      payload.id,
    );
    return { ...user, refreshToken };
  }
}
