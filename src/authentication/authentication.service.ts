import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from 'src/user/user.service';
import * as bcrypt from 'bcrypt';
import { IUser, IUserPayload } from 'src/user/interface/user.interface';
import { ConfigService } from '@nestjs/config';
import { CreateUserDto } from 'src/user/dto/create-user.dto';

@Injectable()
export class AuthenticationService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}

  async validateUser(email: string, pass: string) {
    // find if user exist with this email
    const user = await this.userService.findOneByEmail(email);
    if (!user) {
      throw new NotFoundException('User email was not founded');
    }
    // find if user password match
    const match = await this.comparePassword(pass, user.password);
    if (!match) {
      throw new BadRequestException('Password is not valid');
    }

    return user;
  }

  public async login(user: IUser) {
    const jwtPayload: IUserPayload = {
      id: user.id,
      email: user.email,
    };

    const accesstoken = await this.generateAccessToken(jwtPayload);
    const refreshtoken = await this.generateRefreshToken(jwtPayload);

    await this.userService.setCurrentRefreshToken(refreshtoken, user.id);
    return { jwtPayload, tokens: { accesstoken, refreshtoken } };
  }

  public async create(user: CreateUserDto) {
    // hash the password
    const pass = await this.hashPassword(user.password);

    // create the user
    const newUser = await this.userService.create({ ...user, password: pass });

    // then login
    return await this.login(newUser);
  }

  public async logout(userId: number) {
    // hash the password
    return await this.userService.update(userId, { refreshToken: null });
  }

  /** Private Methods */

  private async generateAccessToken(payload: IUserPayload) {
    const token = await this.jwtService.signAsync(payload, {
      expiresIn: this.configService.get('JWT_ACCESS_EXPIRES'),
      secret: this.configService.get('JWT_ACCESS_SECRET'),
    });
    return token;
  }
  private async generateRefreshToken(payload: IUserPayload) {
    const token = await this.jwtService.signAsync(payload, {
      expiresIn: this.configService.get('JWT_REFRESH_EXPIRES'),
      secret: this.configService.get('JWT_REFRESH_SECRET'),
    });
    return token;
  }

  private async hashPassword(password: string) {
    const hash = await bcrypt.hash(password, 10);
    return hash;
  }

  private async comparePassword(enteredPassword: string, dbPassword: string) {
    const match = await bcrypt.compare(enteredPassword, dbPassword);
    return match;
  }
}
