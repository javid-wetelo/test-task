import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Publicv2Module } from './public/public.module';
import { AdminModule } from './admin/admin.module';
import { AuthenticationModule } from './authentication/authentication.module';
import { ConfigModule } from '@nestjs/config';
import { UserModule } from './user/user.module';
import { DatabaseModule } from './database/database.module';
import { TaskMicroserviceModule } from './taskMicroservice/task.module';
import { RouterModule } from '@nestjs/core';
import { appRoutes } from './app.routes';

@Module({
  imports: [
    Publicv2Module,
    AdminModule,
    RouterModule.register(appRoutes),
    AuthenticationModule,
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    UserModule,
    DatabaseModule,
    TaskMicroserviceModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
