
## Installation

```bash
$ npm install
```
## RabbitMQ integration

In terminal run this command and expose ports 
```bash
$ docker run -d --hostname demo-rabbit -p 5672:5672 -p 15672:15672 --name demo-rabbit rabbitmq:3-management

# Management interface 
http://127.0.0.1:15672
```

## Add .env file and copy/change .env.example file's content in it.


## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```



